import { Component } from '@angular/core';
import { connectableObservableDescriptor } from 'rxjs/internal/observable/ConnectableObservable';

import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  constructor(private storage: Storage) {}

  ngOnInit() {
    this.storage.get('color').then((val) => {
      if(val != null){
        var selected = document.getElementById("selectedColor");
        selected.style.backgroundColor = val;
      }
    });
  }

  setColor(color) {
    console.log(color);
    this.storage.set('color', color);
    var selected = document.getElementById("selectedColor");
    selected.style.backgroundColor = color;
  }
}
